<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\guests;
use Faker\Generator as Faker;

$factory->define(guests::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'location' => $faker->word,
        'guest_of' => $faker->word,
        'favourite_dj' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
