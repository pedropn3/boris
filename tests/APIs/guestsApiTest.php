<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\guests;

class guestsApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_guests()
    {
        $guests = factory(guests::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/guests', $guests
        );

        $this->assertApiResponse($guests);
    }

    /**
     * @test
     */
    public function test_read_guests()
    {
        $guests = factory(guests::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/guests/'.$guests->id
        );

        $this->assertApiResponse($guests->toArray());
    }

    /**
     * @test
     */
    public function test_update_guests()
    {
        $guests = factory(guests::class)->create();
        $editedguests = factory(guests::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/guests/'.$guests->id,
            $editedguests
        );

        $this->assertApiResponse($editedguests);
    }

    /**
     * @test
     */
    public function test_delete_guests()
    {
        $guests = factory(guests::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/guests/'.$guests->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/guests/'.$guests->id
        );

        $this->response->assertStatus(404);
    }
}
