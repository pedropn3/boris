<?php namespace Tests\Repositories;

use App\Models\guests;
use App\Repositories\guestsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class guestsRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var guestsRepository
     */
    protected $guestsRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->guestsRepo = \App::make(guestsRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_guests()
    {
        $guests = factory(guests::class)->make()->toArray();

        $createdguests = $this->guestsRepo->create($guests);

        $createdguests = $createdguests->toArray();
        $this->assertArrayHasKey('id', $createdguests);
        $this->assertNotNull($createdguests['id'], 'Created guests must have id specified');
        $this->assertNotNull(guests::find($createdguests['id']), 'guests with given id must be in DB');
        $this->assertModelData($guests, $createdguests);
    }

    /**
     * @test read
     */
    public function test_read_guests()
    {
        $guests = factory(guests::class)->create();

        $dbguests = $this->guestsRepo->find($guests->id);

        $dbguests = $dbguests->toArray();
        $this->assertModelData($guests->toArray(), $dbguests);
    }

    /**
     * @test update
     */
    public function test_update_guests()
    {
        $guests = factory(guests::class)->create();
        $fakeguests = factory(guests::class)->make()->toArray();

        $updatedguests = $this->guestsRepo->update($fakeguests, $guests->id);

        $this->assertModelData($fakeguests, $updatedguests->toArray());
        $dbguests = $this->guestsRepo->find($guests->id);
        $this->assertModelData($fakeguests, $dbguests->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_guests()
    {
        $guests = factory(guests::class)->create();

        $resp = $this->guestsRepo->delete($guests->id);

        $this->assertTrue($resp);
        $this->assertNull(guests::find($guests->id), 'guests should not exist in DB');
    }
}
