<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/




Route::resource('guests', 'guestsAPIController');
Route::get('guests_dj/{name}','guestsAPIController@guests_dj');
Route::get('top_dj','guestsAPIController@top_dj');
Route::get('location_guests','guestsAPIController@location_guests');