<?php

namespace App\Repositories;

use App\Models\guests;
use App\Repositories\BaseRepository;

/**
 * Class guestsRepository
 * @package App\Repositories
 * @version May 26, 2020, 6:09 pm UTC
*/

class guestsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'location',
        'guest_of',
        'favourite_dj'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return guests::class;
    }
}
