<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateguestsAPIRequest;
use App\Http\Requests\API\UpdateguestsAPIRequest;
use App\Models\guests;
use App\Repositories\guestsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class guestsController
 * @package App\Http\Controllers\API
 */

class guestsAPIController extends AppBaseController
{
    /** @var  guestsRepository */
    private $guestsRepository;

    public function __construct(guestsRepository $guestsRepo)
    {
        $this->guestsRepository = $guestsRepo;
    }

    /**
     * Display a listing of the guests.
     * GET|HEAD /guests
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

        //eloquend
       /* $guests = $this->guestsRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );
        */
        //json read file
        $data = file_get_contents(url("guests.json"));
$guests = json_decode($data, true);
 


        return $this->sendResponse($guests, 'Guests retrieved successfully');
    }

    /**
     * Store a newly created guests in storage.
     * POST /guests
     *
     * @param CreateguestsAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateguestsAPIRequest $request)
    {
        $input = $request->all();

        $guests = $this->guestsRepository->create($input);

        return $this->sendResponse($guests->toArray(), 'Guests saved successfully');
    }

    /**
     * Display the specified guests.
     * GET|HEAD /guests/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var guests $guests */
        $guests = $this->guestsRepository->find($id);

        if (empty($guests)) {
            return $this->sendError('Guests not found');
        }

        return $this->sendResponse($guests->toArray(), 'Guests retrieved successfully');
    }

    /**
     * Update the specified guests in storage.
     * PUT/PATCH /guests/{id}
     *
     * @param int $id
     * @param UpdateguestsAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateguestsAPIRequest $request)
    {
        $input = $request->all();

        /** @var guests $guests */
        $guests = $this->guestsRepository->find($id);

        if (empty($guests)) {
            return $this->sendError('Guests not found');
        }

        $guests = $this->guestsRepository->update($input, $id);

        return $this->sendResponse($guests->toArray(), 'guests updated successfully');
    }

    /**
     * Remove the specified guests from storage.
     * DELETE /guests/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */


    



    public function guests_dj ($name_dj){
        /* 
        $mas=guests::where("guest_of","=",$name_dj)->count();
        return $this->sendResponse($mas, 'number guests for dj successfully');
            */
            $data = file_get_contents(url("guests.json"));
            $guests = json_decode($data, true);
            $i=0;
            foreach($guests as $item){
              if($item['guest_of']==$name_dj){
                  $i=$i+1;
              }
             
            }
            return $this->sendResponse($i, 'number guests for dj '.$name_dj.' successfully');
    }


    public function top_dj (){

       /* $mas=guests::select("favourite_dj")->get();
        $mm=$mas->toArray();

        */
       
        $data = file_get_contents(url("guests.json"));
        $guests = json_decode($data, true);
         


        foreach($guests as $val){
            unset($val['name'],$val['location'],$val['guest_of']);
            $hash = $val['favourite_dj'];
            if(isset($result[$hash])){
                $result[$hash]['count'] += $result[$hash]['count'];
            }else{
                $result[$hash] = $val;
                $result[$hash]['count'] = 1;
            }
        }




        return $this->sendResponse($result, 'TOP DJ successfully');

    }





    public function location_guests (){

        /*
        $mas=guests::select("location")->get();
        $mm=$mas->toArray();
    */

           
    $data = file_get_contents(url("guests.json"));
    $guests = json_decode($data, true);


        foreach($guests as $val){
            unset($val['name'],$val['favourite_dj'],$val['guest_of']);
            $hash = $val['location'];
            if(isset($result[$hash])){
                $result[$hash]['count'] += $result[$hash]['count'];
            }else{
                $result[$hash] = $val;
                $result[$hash]['count'] = 1;
            }
        }




        return $this->sendResponse($result, 'location guests successfully');

    }




    public function destroy($id)
    {
        /** @var guests $guests */
        $guests = $this->guestsRepository->find($id);

        if (empty($guests)) {
            return $this->sendError('Guests not found');
        }

        $guests->delete();

        return $this->sendSuccess('Guests deleted successfully');
    }
}
