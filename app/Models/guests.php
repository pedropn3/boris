<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class guests
 * @package App\Models
 * @version May 26, 2020, 6:09 pm UTC
 *
 * @property string $name
 * @property string $location
 * @property string $guest_of
 * @property string $favourite_dj
 */
class guests extends Model
{
    use SoftDeletes;

    public $table = 'guests';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'location',
        'guest_of',
        'favourite_dj'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'location' => 'string',
        'guest_of' => 'string',
        'favourite_dj' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
